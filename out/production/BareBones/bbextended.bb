init x = 27;
init y = 12;
init z = 214;

main;
hlt;

proc main;

    while x not 0;
        x--;
        if x == 13;
            multiply!;
	   break;
        endif;
    end;

ret;

proc multiply!;

    z = x * y;
    z = z * z;

ret;