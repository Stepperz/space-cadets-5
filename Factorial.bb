#Factorial calculator

#Factorial to calculate
init factorial = 11;
init current = 1;
init result = 1;

main;
hlt;

proc run;
    while factorial not 0 do;
        result = result * current;
        current++;

        factorial--;
    end;
ret;

proc main;
    run;
ret;