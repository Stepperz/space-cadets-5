package IDE;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

/**
 * Created by Ollie on 01/11/2014.
 */
public class VariableTable{

    private JScrollPane scrollPane;
    private HashMap<String, Integer> variables;
    private JTable table;

    public VariableTable(){
        String[] columnNames = {"Variable", "Value"};
        table = new JTable(new Object[][]{}, columnNames);
        scrollPane = new JScrollPane(table);
        scrollPane.setMinimumSize(new Dimension(160, 300));
        scrollPane.setPreferredSize(new Dimension(200, 600));
    }

    public JScrollPane getScrollPane(){
        return scrollPane;
    }

    public void setData(HashMap<String, Integer> variables){
        scrollPane.remove(table);

        this.variables = variables;
        Object[][] data = new Object[variables.keySet().size()][2];
        int index = 0;
        for(String s : variables.keySet()){
            Integer i = variables.get(s);
            data[index][0] = s;
            data[index][1] = i;
            index++;
        }

        String[] columnNames = {"Variable", "Value"};
        table = new JTable(data, columnNames);
        scrollPane = new JScrollPane(table);
        scrollPane.setMinimumSize(new Dimension(160, 300));
        scrollPane.setPreferredSize(new Dimension(200, 600));
    }

}
