package IDE;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Ollie on 31/10/2014.
 */
public class MenuBar {

    private JMenuBar menuBar;

    public MenuBar(){
        menuBar = new JMenuBar();
        menuBar.setOpaque(true);
        menuBar.setPreferredSize(new Dimension(800, 25));
    }

    public JMenuBar getJMenuBar(){
        return menuBar;
    }

    public void addMenu(JMenu menu){
        menuBar.add(menu);
    }

}
