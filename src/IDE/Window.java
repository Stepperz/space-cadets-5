package IDE;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Ollie on 31/10/2014.
 */
public class Window extends JFrame {

    JPanel contentPane;

    public Window(String title){
        setTitle(title);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        setLookAndFeel();

        contentPane = new JPanel(new BorderLayout());
    }

    public void setMenuBar(MenuBar mb){
        setJMenuBar(mb.getJMenuBar());
    }

    public void display(){
        pack();
        setVisible(true);
    }

    private void setLookAndFeel(){
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
    }

}
