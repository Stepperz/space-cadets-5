package IDE;

import Interpreter.Interpreter;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

/**
 * Created by Ollie on 01/11/2014.
 */
public class IDE extends JFrame{

    private JSplitPane upperSplitPane;
    private JSplitPane lowerSplitPane;
    private JTabbedPane tabbedPane;
    private FileEditorPane currentFilePane;
    private VariableTable variableTable;
    private EditorOutput editorOutput;
    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenu runMenu;
    private JMenu editMenu;

    private JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir"));

    private Action Save;
    private Action SaveAs;

    private Interpreter interpreter;

    public IDE() {
        super("Bare Bones IDE");
        setMinimumSize(new Dimension(600, 500));
        JPopupMenu.setDefaultLightWeightPopupEnabled(false);

        tabbedPane = new JTabbedPane();
        tabbedPane.setMinimumSize(new Dimension(300, 300));
        tabbedPane.setPreferredSize(new Dimension(800, 600));
        FileEditorPane editPane = new FileEditorPane();
        tabbedPane.addTab(editPane.getFileName(), new ImageIcon("images/barebones.gif"), editPane);
        currentFilePane = editPane;
        Save = editPane.Save;
        SaveAs = editPane.SaveAs;
        Save.setEnabled(false);

        variableTable = new VariableTable();

        upperSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, tabbedPane, variableTable.getScrollPane());
        upperSplitPane.setContinuousLayout(true);
        upperSplitPane.setDividerSize(3);
        upperSplitPane.setFocusable(false);

        editorOutput = new EditorOutput();

        lowerSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, upperSplitPane, editorOutput.getScrollPane());
        lowerSplitPane.setContinuousLayout(true);
        lowerSplitPane.setDividerSize(3);
        lowerSplitPane.setFocusable(false);

        getContentPane().add(lowerSplitPane);

        menuBar = new JMenuBar();
        fileMenu = createFileMenu();
        editMenu = createEditMenu();
        runMenu = createRunMenu();
        menuBar.add(fileMenu);
        menuBar.add(editMenu);
        menuBar.add(runMenu);
        setJMenuBar(menuBar);

        tabbedPane.addChangeListener(changeListener);
    }

    private JMenu createFileMenu(){
        JMenu menu = new JMenu("File");

        menu.add(New);
        menu.add(Open);
        menu.add(Save);
        menu.getItem(2).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Event.CTRL_MASK));
        menu.add(SaveAs);
        menu.add(Close);
        menu.addSeparator();
        menu.add(Exit);

        return menu;
    }

    private JMenu createEditMenu(){
        JMenu menu = new JMenu("Edit");
        menu.add(currentFilePane.getActionMap().get(DefaultEditorKit.cutAction));
        menu.add(currentFilePane.getActionMap().get(DefaultEditorKit.copyAction));
        menu.add(currentFilePane.getActionMap().get(DefaultEditorKit.pasteAction));
        menu.addSeparator();
        menu.add(currentFilePane.getActionMap().get("Undo"));
        menu.add(currentFilePane.getActionMap().get("Redo"));
        menu.getItem(1).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, Event.CTRL_MASK));
        menu.getItem(2).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK));
        menu.getItem(4).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, Event.CTRL_MASK));
        menu.getItem(5).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, Event.CTRL_MASK | InputEvent.SHIFT_MASK));
        menu.addSeparator();
        menu.add(currentFilePane.getActionMap().get(DefaultEditorKit.selectAllAction));

        menu.getItem(0).setText("Cut");
        menu.getItem(1).setText("Copy");
        menu.getItem(2).setText("Paste");
        menu.getItem(4).setText("Undo");
        menu.getItem(5).setText("Redo");
        menu.getItem(7).setText("Select All");

        return menu;
    }

    private JMenu createRunMenu(){
        JMenu menu = new JMenu("Run");
        menu.add(Run);
        menu.add(Debug);
        menu.addSeparator();
        menu.add(Continue);
        menu.add(Stop);

        menu.getItem(0).setText("Run " + currentFilePane.getFileName());
        menu.getItem(1).setText("Debug " + currentFilePane.getFileName());
        menu.getItem(1).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F9, Event.SHIFT_MASK));
        menu.getItem(3).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F9, 0));

        Debug.setEnabled(true);
        Continue.setEnabled(false);
        Stop.setEnabled(false);

        return menu;
    }

    private void SaveAll(){

    }

    private ChangeListener changeListener = new ChangeListener() {
        public void stateChanged(ChangeEvent changeEvent) {
            int i = tabbedPane.getSelectedIndex();
            if (i != -1) {
                currentFilePane = (FileEditorPane)tabbedPane.getComponentAt(i);
                tabbedPane.setTitleAt(i, currentFilePane.getFileName());
                Save = currentFilePane.Save;
                SaveAs = currentFilePane.SaveAs;
                fileMenu.getItem(2).setAction(Save);
                fileMenu.getItem(3).setAction(SaveAs);
                runMenu.getItem(0).setText("Run " + currentFilePane.getFileName());
                runMenu.getItem(1).setText("Debug " + currentFilePane.getFileName());
                editMenu.getItem(4).setAction(currentFilePane.getActionMap().get("Undo"));
                editMenu.getItem(5).setAction(currentFilePane.getActionMap().get("Redo"));
            }
            fileMenu.getItem(2).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Event.CTRL_MASK));
            editMenu.getItem(1).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, Event.CTRL_MASK));
            editMenu.getItem(2).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK));
            editMenu.getItem(4).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, Event.CTRL_MASK));
            editMenu.getItem(5).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, Event.CTRL_MASK | InputEvent.SHIFT_MASK));
        }
    };

    Action Debug = new AbstractAction("Debug") {
        public void actionPerformed(ActionEvent e) {
            if(!currentFilePane.getFileName().equals("Untitled"))
                currentFilePane.saveFile(currentFilePane.getFilePath(), currentFilePane.getFileName());
            else
                currentFilePane.saveFileAs();

            editorOutput.clearOutput();
            editorOutput.println("INFO: Debugging " + currentFilePane.getFileName());

            if(currentFilePane.getFilePath().equals("null")) {
                editorOutput.println("INFO: File not chosen, debug aborted.");
                return;
            }

            try {
                interpreter = new Interpreter(currentFilePane.getFilePath(), editorOutput);
                interpreter.debug(currentFilePane.getBreakPoints());
                variableTable.setData(interpreter.getVariables());
                upperSplitPane.setRightComponent(variableTable.getScrollPane());
                if(!interpreter.isFinished() && !interpreter.hasFailed()) {
                    editorOutput.println("DEBUG: Breakpoint hit on line " + interpreter.getSrcLineNumber());
                    Debug.setEnabled(false);
                    Continue.setEnabled(true);
                    Stop.setEnabled(true);
                }else{
                    if(interpreter.hasFailed()){
                        editorOutput.println("INFO: Program aborted.");
                        Debug.setEnabled(true);
                        Continue.setEnabled(false);
                        Stop.setEnabled(false);
                    }else {
                        editorOutput.println("INFO: Program complete.");
                        Debug.setEnabled(true);
                        Continue.setEnabled(false);
                        Stop.setEnabled(false);
                    }
                }
            } catch (java.io.IOException e1) {
                e1.printStackTrace();
            }
        }
    };

    Action Continue = new AbstractAction("Continue") {
        public void actionPerformed(ActionEvent e) {
            interpreter.continueDebug(currentFilePane.getBreakPoints());
            variableTable.setData(interpreter.getVariables());
            upperSplitPane.setRightComponent(variableTable.getScrollPane());
            if(!interpreter.isFinished()) {
                editorOutput.println("DEBUG: Breakpoint hit on line " + interpreter.getSrcLineNumber());
                Debug.setEnabled(false);
                Continue.setEnabled(true);
                Stop.setEnabled(true);
            }else{
                if(interpreter.hasFailed()){
                    editorOutput.println("INFO: Program failure.");
                    Debug.setEnabled(true);
                    Continue.setEnabled(false);
                    Stop.setEnabled(false);
                }else {
                    editorOutput.println("INFO: Program complete.");
                    Debug.setEnabled(true);
                    Continue.setEnabled(false);
                    Stop.setEnabled(false);
                }
            }
        }
    };

    Action Stop = new AbstractAction("Stop") {
        public void actionPerformed(ActionEvent e) {
            Debug.setEnabled(true);
            Continue.setEnabled(false);
            Stop.setEnabled(false);
        }
    };

    Action Run = new AbstractAction("Run") {
        public void actionPerformed(ActionEvent e) {
            if(!currentFilePane.getFileName().equals("Untitled"))
                currentFilePane.saveFile(currentFilePane.getFilePath(), currentFilePane.getFileName());
            else
                currentFilePane.saveFileAs();

            editorOutput.clearOutput();
            editorOutput.println("INFO: Starting " + currentFilePane.getFileName());

            if(currentFilePane.getFilePath().equals("null")) {
                editorOutput.println("INFO: File not chosen, run aborted.");
                return;
            }

            try {
                interpreter = new Interpreter(currentFilePane.getFilePath(), editorOutput);
                interpreter.run();
                variableTable.setData(interpreter.getVariables());
                upperSplitPane.setRightComponent(variableTable.getScrollPane());
                if(interpreter.hasFailed()){
                    editorOutput.println("INFO: Program aborted.");
                    Debug.setEnabled(true);
                    Continue.setEnabled(false);
                    Stop.setEnabled(false);
                }else {
                    editorOutput.println("INFO: Program complete.");
                    Debug.setEnabled(true);
                    Continue.setEnabled(false);
                    Stop.setEnabled(false);
                }
            } catch (java.io.IOException e1) {
                e1.printStackTrace();
            }
        }
    };

    Action New = new AbstractAction("New", new ImageIcon("images/new.gif")) {
        public void actionPerformed(ActionEvent e) {
            FileEditorPane editPane = new FileEditorPane();
            tabbedPane.addTab(editPane.getFileName(), new ImageIcon("images/barebones.gif"), editPane);
            currentFilePane = editPane;
            tabbedPane.setSelectedComponent(editPane);
            Save.setEnabled(false);
        }
    };

    Action Open = new AbstractAction("Open", new ImageIcon("images/open.gif")) {
        public void actionPerformed(ActionEvent e) {
            if(fileChooser.showOpenDialog(null)==JFileChooser.APPROVE_OPTION) {
                FileEditorPane editPane = new FileEditorPane(fileChooser.getSelectedFile().getAbsolutePath(), fileChooser.getSelectedFile().getAbsoluteFile().getName());
                tabbedPane.addTab(editPane.getFileName(), new ImageIcon("images/barebones.gif"), editPane);
                currentFilePane = editPane;
                tabbedPane.setSelectedComponent(editPane);
            }
        }
    };

    Action Close = new AbstractAction("Close") {
        public void actionPerformed(ActionEvent e) {
            if(Save.isEnabled())
                currentFilePane.saveFileAs();

            tabbedPane.remove(currentFilePane);
            if(tabbedPane.getTabCount() == 0)
                currentFilePane = null;
            else {
                tabbedPane.setSelectedIndex(0);
                currentFilePane = (FileEditorPane)tabbedPane.getSelectedComponent();
            }
        }
    };

    Action Exit = new AbstractAction("Quit") {
        public void actionPerformed(ActionEvent e) {
            SaveAll();
            System.exit(0);
        }
    };

    private static void createAndShowGUI() {
        final IDE ide = new IDE();
        ide.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ide.pack();
        ide.setVisible(true);
    }

    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                //Turn off metal's use of bold fonts
                UIManager.put("swing.boldMetal", Boolean.FALSE);
                createAndShowGUI();
            }
        });
    }
}
