package IDE;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Ollie on 02/11/2014.
 */
public class EditorOutput {

    private JScrollPane scrollPane;
    private JTextArea textArea;

    public EditorOutput(){
        textArea = new JTextArea();
        textArea.setEditable(false);
        textArea.setCaretPosition(0);
        textArea.setMargin(new Insets(5, 5, 5, 5));
        textArea.setBackground(Color.DARK_GRAY);
        textArea.setForeground(Color.WHITE);
        textArea.setFont(new Font("Monospaced", Font.BOLD, 14));
        scrollPane = new JScrollPane(textArea);
        scrollPane.setMinimumSize(new Dimension(800, 100));
        scrollPane.setPreferredSize(new Dimension(800, 200));
        scrollPane.setViewportBorder(BorderFactory.createTitledBorder("Console"));
    }

    public JScrollPane getScrollPane(){
        return scrollPane;
    }

    public void println(String line){
        textArea.append(line + "\n");
    }

    public void clearOutput(){
        textArea.setText("");
    }

}
