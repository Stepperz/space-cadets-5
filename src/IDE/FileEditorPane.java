package IDE;

import com.sun.xml.internal.ws.api.streaming.XMLStreamReaderFactory;
import javafx.scene.control.RadioButton;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.event.UndoableEditEvent;
import javax.swing.text.*;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;
import java.awt.*;
import java.awt.event.*;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Ollie on 01/11/2014.
 */
public class FileEditorPane extends ScrollPane {

    private JTextPane textPane;
    private JPanel panel;
    private JTextArea lineNumbers;
    private JTextPane breakPointPane;
    private int lines;
    private DefaultStyledDocument doc;
    private String fileName = "Untitled";
    private String filePath = "null";
    private ActionMap actionMap;
    private ArrayList<Integer> breakpoints = new ArrayList<>();

    private UndoAction undoAction = new UndoAction();
    private RedoAction redoAction = new RedoAction();
    private UndoManager undo = new UndoManager();

    private JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir"));

    private boolean changedName = false;
    private boolean modified = false;

    //Syntax Highlighting styles
    StyleContext context = new StyleContext();
    Style styleKeyword = context.addStyle("keyword", null);
    Style styleWord = context.addStyle("word", null);
    Style styleNumber = context.addStyle("number", null);
    Style styleComment = context.addStyle("comment", null);

    public FileEditorPane(){
        super(SCROLLBARS_AS_NEEDED);

        panel = new JPanel();

        textPane = new JTextPane();
        textPane.setCaretPosition(0);
        textPane.setMargin(new Insets(5, 5, 5, 5));
        textPane.setBackground(Color.DARK_GRAY);
        textPane.setForeground(Color.WHITE);
        textPane.setFont(new Font("Monospaced", Font.PLAIN, 14));
        textPane.setAutoscrolls(true);
        textPane.setCaretColor(Color.white);
        doc = (DefaultStyledDocument)textPane.getStyledDocument();
        StyleConstants.setForeground(styleKeyword, Color.ORANGE);
        StyleConstants.setForeground(styleWord, Color.WHITE);
        StyleConstants.setForeground(styleNumber, Color.CYAN);
        StyleConstants.setForeground(styleComment, Color.GREEN);
        syntaxHighlightWholeDocument(doc);

        try {
            String document = doc.getText(0, doc.getLength());
            lines = document.split("\n").length;
        } catch (BadLocationException e) {
            e.printStackTrace();
        }

        lineNumbers = new JTextArea();
        lineNumbers.setEditable(false);
        lineNumbers.setBackground(Color.GRAY);
        lineNumbers.setForeground(Color.WHITE);
        lineNumbers.setFont(new Font("Monospaced", Font.PLAIN, 14));
        lineNumbers.setPreferredSize(new Dimension(lineNumbers.getFont().getSize()*Integer.toString(lines).length(), 600));
        lineNumbers.setMaximumSize(new Dimension(lineNumbers.getFont().getSize()*Integer.toString(lines).length(), Integer.MAX_VALUE));
        lineNumbers.setMargin(new Insets(5,4,5,4));
        lineNumbers.setAlignmentX(TextArea.RIGHT_ALIGNMENT);
        lineNumbers.setAutoscrolls(true);
        for(int i = 1; i <= lines; i++){
            lineNumbers.append(i + "\n");
        }

        breakPointPane = new JTextPane();
        breakPointPane.setEditable(false);
        breakPointPane.setMargin(new Insets(5,4,5,4));
        breakPointPane.setBackground(Color.GRAY);
        breakPointPane.setFont(new Font("Monospaced", Font.PLAIN, 14));
        breakPointPane.setMinimumSize(new Dimension(breakPointPane.getFont().getSize(), 300));
        breakPointPane.setMaximumSize(new Dimension(breakPointPane.getFont().getSize(), Integer.MAX_VALUE));
        breakPointPane.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
        DefaultStyledDocument bpDoc = (DefaultStyledDocument)breakPointPane.getStyledDocument();
        for(int i = 0; i < lines; i++){
            JRadioButton rb = new JRadioButton();
            rb.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        breakpoints.add((Integer) ((JRadioButton) e.getItem()).getClientProperty("num"));
                    } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                        breakpoints.remove(((JRadioButton) e.getItem()).getClientProperty("num"));
                    }
                }
            });
            rb.putClientProperty("num", i + 1);
            rb.setAlignmentY(0.75f);
            rb.setSelected(false);
            rb.setVisible(true);
            rb.setMargin(new Insets(0, 0, 0, 0));
            rb.setBounds(0, 0, 12, 12);
            rb.setBackground(Color.GRAY);
            breakPointPane.insertComponent(rb);
            try {
                bpDoc.insertString(bpDoc.getLength(), "\n", null);
            } catch (BadLocationException e) {
                e.printStackTrace();
            }
        }

        BoxLayout layout = new BoxLayout(panel, BoxLayout.X_AXIS);
        panel.setLayout(layout);
        panel.add(lineNumbers);
        panel.add(breakPointPane);
        panel.add(textPane);
        panel.setAutoscrolls(true);

        add(panel);

        actionMap = textPane.getActionMap();
        actionMap.put("Undo", undoAction);
        actionMap.put("Redo", redoAction);
        actionMap.put("Save", Save);

        doc.addUndoableEditListener(new UndoableEditListener());
        textPane.addKeyListener(k1);
    }

    public FileEditorPane(String filePath, String fileName){
        super(SCROLLBARS_AS_NEEDED);

        panel = new JPanel();

        textPane = new JTextPane();
        textPane.setCaretPosition(0);
        textPane.setMargin(new Insets(5, 5, 5, 5));
        textPane.setBackground(Color.DARK_GRAY);
        textPane.setForeground(Color.WHITE);
        textPane.setFont(new Font("Monospaced", Font.PLAIN, 14));
        textPane.setAutoscrolls(true);
        textPane.setCaretColor(Color.white);
        readInFile(filePath, fileName);

        lineNumbers = new JTextArea();
        lineNumbers.setEditable(false);
        lineNumbers.setBackground(Color.GRAY);
        lineNumbers.setForeground(Color.WHITE);
        lineNumbers.setFont(new Font("Monospaced", Font.PLAIN, 14));
        lineNumbers.setPreferredSize(new Dimension(lineNumbers.getFont().getSize()*Integer.toString(lines).length(), 600));
        lineNumbers.setMaximumSize(new Dimension(lineNumbers.getFont().getSize()*Integer.toString(lines).length(), Integer.MAX_VALUE));
        lineNumbers.setMargin(new Insets(5,4,5,4));
        lineNumbers.setAlignmentX(TextArea.RIGHT_ALIGNMENT);
        lineNumbers.setAutoscrolls(true);
        for(int i = 1; i <= lines; i++){
            lineNumbers.append(i + "\n");
        }

        breakPointPane = new JTextPane();
        breakPointPane.setEditable(false);
        breakPointPane.setMargin(new Insets(5, 4, 5, 4));
        breakPointPane.setBackground(Color.GRAY);
        breakPointPane.setFont(new Font("Monospaced", Font.PLAIN, 14));
        breakPointPane.setMinimumSize(new Dimension(breakPointPane.getFont().getSize(), 300));
        breakPointPane.setMaximumSize(new Dimension(breakPointPane.getFont().getSize(), Integer.MAX_VALUE));
        breakPointPane.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
        DefaultStyledDocument bpDoc = (DefaultStyledDocument)breakPointPane.getStyledDocument();
        for(int i = 0; i < lines; i++){
            JRadioButton rb = new JRadioButton();
            rb.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        breakpoints.add((Integer) ((JRadioButton) e.getItem()).getClientProperty("num"));
                    } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                        breakpoints.remove(((JRadioButton) e.getItem()).getClientProperty("num"));
                    }
                }
            });
            rb.putClientProperty("num", i+1);
            rb.setAlignmentY(0.75f);
            rb.setSelected(false);
            rb.setVisible(true);
            rb.setMargin(new Insets(0,0,0,0));
            rb.setBounds(0, 0, 12, 12);
            rb.setBackground(Color.GRAY);
            breakPointPane.insertComponent(rb);
            try {
                bpDoc.insertString(bpDoc.getLength(), "\n", null);
            } catch (BadLocationException e) {
                e.printStackTrace();
            }
        }

        BoxLayout layout = new BoxLayout(panel, BoxLayout.X_AXIS);
        panel.setLayout(layout);
        panel.add(lineNumbers);
        panel.add(breakPointPane);
        panel.add(textPane);
        panel.setAutoscrolls(true);

        add(panel);

        setPreferredSize(new Dimension(800, 600));

        actionMap = textPane.getActionMap();
        actionMap.put("Paste", Paste);
        actionMap.put("Undo", undoAction);
        actionMap.put("Redo", redoAction);
        actionMap.put("Save", Save);

        textPane.addKeyListener(k1);

        Save.setEnabled(false);

    }

    private KeyListener k1 = new KeyAdapter() {
        public void keyPressed(KeyEvent e) {
            Save.setEnabled(true);
            SaveAs.setEnabled(true);
            int pos = textPane.getCaretPosition();
            if(pos > 0)
                modified = true;

            int oldLines = lines;
            try {
                String document = doc.getText(0, doc.getLength());
                lines = document.split("\n").length;
            } catch (BadLocationException ex) {
                ex.printStackTrace();
            }
            if(oldLines != lines) {
                lineNumbers.setText("");
                for (int i = 1; i <= lines; i++) {
                    lineNumbers.append(i + "\n");
                }
                lineNumbers.setPreferredSize(new Dimension(lineNumbers.getFont().getSize() * Integer.toString(lines).length(), 600));
                lineNumbers.setMaximumSize(new Dimension(lineNumbers.getFont().getSize() * Integer.toString(lines).length(), Integer.MAX_VALUE));

                updateBreakPoints();
            }
        }

        @Override
        public void keyTyped(KeyEvent e) {
            super.keyTyped(e);
            if(modified){
                syntaxHighlightChanges(textPane, doc);
                modified = false;
            }
        }
    };

    public void updateBreakPoints(){
        panel.remove(breakPointPane);
        panel.remove(textPane);

        breakPointPane = new JTextPane();
        breakPointPane.setEditable(false);
        breakPointPane.setMargin(new Insets(5,4,5,4));
        breakPointPane.setBackground(Color.GRAY);
        breakPointPane.setFont(new Font("Monospaced", Font.PLAIN, 14));
        breakPointPane.setMinimumSize(new Dimension(breakPointPane.getFont().getSize(), 300));
        breakPointPane.setMaximumSize(new Dimension(breakPointPane.getFont().getSize(), Integer.MAX_VALUE));
        breakPointPane.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
        DefaultStyledDocument bpDoc = (DefaultStyledDocument)breakPointPane.getStyledDocument();

        for(int i = 0; i < lines; i++){
            JRadioButton rb = new JRadioButton();
            rb.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        breakpoints.add((Integer) ((JRadioButton) e.getItem()).getClientProperty("num"));
                    } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                        breakpoints.remove(((JRadioButton) e.getItem()).getClientProperty("num"));
                    }
                }
            });
            rb.putClientProperty("num", i + 1);
            rb.setAlignmentY(0.75f);
            rb.setSelected(false);
            rb.setVisible(true);
            rb.setMargin(new Insets(0, 3, 0, 0));
            rb.setBounds(0, 0, 12, 12);
            rb.setBackground(Color.GRAY);
            breakPointPane.insertComponent(rb);
            try {
                bpDoc.insertString(bpDoc.getLength(), "\n", null);
            } catch (BadLocationException e) {
                e.printStackTrace();
            }
        }
        panel.add(breakPointPane);
        panel.add(textPane);
        textPane.grabFocus();
    }

    public ActionMap getActionMap(){
        return actionMap;
    }

    public String getFileName(){
        return fileName;
    }

    public String getFilePath(){
        return filePath;
    }

    public int[] getBreakPoints(){
        int[] r = new int[breakpoints.size()];
        for(int i = 0; i < breakpoints.size(); i++){
            r[i] = breakpoints.get(i);
        }
        return r;
    }

    public boolean hasChangedName(){
        return changedName;
    }

    private class UndoableEditListener implements javax.swing.event.UndoableEditListener {
        @Override
        public void undoableEditHappened(UndoableEditEvent e) {
            if(e.getEdit().getPresentationName().equals("style change"))
                return;
            undo.addEdit(e.getEdit());
            undoAction.updateUndoState();
            redoAction.updateRedoState();
        }
    }

    class UndoAction extends AbstractAction {
        public UndoAction() {
            super("Undo");
            setEnabled(false);
        }

        public void actionPerformed(ActionEvent e) {
            try {
                undo.undo();
            } catch (CannotUndoException ex) {
                System.out.println("Unable to undo: " + ex);
                ex.printStackTrace();
            }
            updateUndoState();
            redoAction.updateRedoState();
        }

        protected void updateUndoState() {
            if (undo.canUndo()) {
                setEnabled(true);
                putValue(Action.NAME, undo.getUndoPresentationName());
            } else {
                setEnabled(false);
                putValue(Action.NAME, "Undo");
            }
        }
    }

    class RedoAction extends AbstractAction {
        public RedoAction() {
            super("Redo");
            setEnabled(false);
        }

        public void actionPerformed(ActionEvent e) {
            try {
                undo.redo();
            } catch (CannotRedoException ex) {
                System.out.println("Unable to redo: " + ex);
                ex.printStackTrace();
            }
            updateRedoState();
            undoAction.updateUndoState();
        }

        protected void updateRedoState() {
            if (undo.canRedo()) {
                setEnabled(true);
                putValue(Action.NAME, undo.getRedoPresentationName());
            } else {
                setEnabled(false);
                putValue(Action.NAME, "Redo");
            }
        }
    }

    private void readInFile(String filePath, String fileName) {
        try {
            FileReader r = new FileReader(filePath);
            textPane.read(r,null);
            r.close();
            doc = (DefaultStyledDocument)textPane.getStyledDocument();
            StyleConstants.setForeground(styleKeyword, Color.ORANGE);
            StyleConstants.setForeground(styleWord, Color.WHITE);
            StyleConstants.setForeground(styleNumber, Color.CYAN);
            StyleConstants.setForeground(styleComment, Color.GREEN);
            syntaxHighlightWholeDocument(doc);
            doc.addUndoableEditListener(new UndoableEditListener());

            try {
                String document = doc.getText(0, doc.getLength());
                lines = document.split("\n").length;
            } catch (BadLocationException e) {
                e.printStackTrace();
            }

            this.fileName = fileName;
            this.filePath = filePath;
        }
        catch(IOException e) {
            Toolkit.getDefaultToolkit().beep();
            JOptionPane.showMessageDialog(null,"Editor can't find the file called "+filePath);
        }
    }

    public void saveFile(String filePath, String fileName){
        if(!filePath.endsWith(".bb")) {
            filePath += ".bb";
            fileName += ".bb";
        }
        try {
            FileWriter w = new FileWriter(filePath);
            textPane.write(w);
            w.close();
            if(!fileName.equals(this.fileName)) {
                this.fileName = fileName;
                this.filePath = filePath;
                changedName = true;
            }
            Save.setEnabled(false);
        }
        catch(IOException e) {
        }
    }

    public void saveFileAs() {
        if(fileChooser.showSaveDialog(null)==JFileChooser.APPROVE_OPTION) {
            saveFile(fileChooser.getSelectedFile().getAbsolutePath(), fileChooser.getSelectedFile().getAbsoluteFile().getName());
            Save.setEnabled(false);
        }
    }

    public Action Save = new AbstractAction("Save", new ImageIcon("images/save.gif")) {
        public void actionPerformed(ActionEvent e) {
            if(!fileName.equals("Untitled"))
                saveFile(filePath, fileName);
            else
                saveFileAs();
        }
    };

    public Action SaveAs = new AbstractAction("Save as...") {
        public void actionPerformed(ActionEvent e) {
            saveFileAs();
        }
    };

    public Action Paste = new AbstractAction("Paste") {
        @Override
        public void actionPerformed(ActionEvent e) {
            syntaxHighlightWholeDocument(doc);
        }
    };

    public void syntaxHighlightChanges(JTextPane textPane, DefaultStyledDocument doc){
        String text = null;
        try {
            text = doc.getText(0, doc.getLength());
        } catch (BadLocationException e) {
            e.printStackTrace();
        }

        String current;
        boolean comment = false;
        boolean number = false;
        int lineStart = -1;
        //calculate the line start
        for (int i = textPane.getCaretPosition(); i >= 1; i--) {
            current = String.valueOf(text.charAt(i-1));
            if (current.equals("\n")) {
                lineStart = i + 1;
                //calculate the first character
                for (int j = lineStart; j < textPane.getCaretPosition(); j++) {
                    current = String.valueOf(text.charAt(j-1));
                    if (!current.matches("\\w")) {
                        if (current.equals("#")) {
                            comment = true;
                            break;
                        }
                    }
                }
                break;
            }
        }
        if (lineStart == -1) {
            for (int i = 1; i < textPane.getCaretPosition(); i++) {
                current = String.valueOf(text.charAt(i-1));
                if (!current.matches("\\w")) {
                    if (current.equals("#")) {
                        comment = true;
                        break;
                    }
                }
            }
            lineStart = 0;
        }

        int lineEnd = -1;
        if (comment) {
            //calculate the line end
            for (int i = textPane.getCaretPosition(); i < text.length(); i++) {
                current = String.valueOf(text.charAt(i-1));
                if (current.equals("\n")) {
                    lineEnd = i;
                    break;
                }
            }
            if (lineEnd != -1) {
                doc.setCharacterAttributes(lineStart, lineEnd - lineStart, styleComment, false);
            } else {
                doc.setCharacterAttributes(lineStart, text.length() - lineStart, styleComment, false);
            }
            return;
        }

        int wordStart = -1;
        //calculate the word start
        for (int i = textPane.getCaretPosition(); i >= 1; i--) {
            current = String.valueOf(text.charAt(i-1));
            if (current.matches("\\s") || current.equals("\n")) {
                wordStart = i;
                break;
            }
        }
        if (wordStart < 0) {
            wordStart = 0;
        }

        int wordEnd = -1;
        //calculate the word end
        for (int i = textPane.getCaretPosition(); i < text.length(); i++) {
            current = String.valueOf(text.charAt(i));
            if (current.matches("\\s")) {
                wordEnd = i;
                break;
            }else if(current.equals("\n")){
                wordEnd = i-1;
                break;
            }
        }
        if (wordEnd == -1) {
            if(text.length() == 0)
                wordEnd = 0;
            else
                wordEnd = text.length();
        }

        current = text.substring(wordStart, wordEnd);

        if(current.endsWith(";")) {
            doc.setCharacterAttributes(wordEnd - 1, 1, styleKeyword, false);
            current = current.substring(0, current.length()-1);
        }

        try {
            Integer.parseInt(current);
            number = true;
        }catch(NumberFormatException e){

        }

        switch (current) {
            default:
                if (number)
                    doc.setCharacterAttributes(wordStart, current.length(), styleNumber, false);
                else
                    doc.setCharacterAttributes(wordStart, current.length(), styleWord, false);
                break;
            case "clear":
                doc.setCharacterAttributes(wordStart, current.length(), styleKeyword, false);
                break;
            case "init":
                doc.setCharacterAttributes(wordStart, current.length(), styleKeyword, false);
                break;
            case "copy":
                doc.setCharacterAttributes(wordStart, current.length(), styleKeyword, false);
                break;
            case "incr":
                doc.setCharacterAttributes(wordStart, current.length(), styleKeyword, false);
                break;
            case "decr":
                doc.setCharacterAttributes(wordStart, current.length(), styleKeyword, false);
                break;
            case "proc":
                doc.setCharacterAttributes(wordStart, current.length(), styleKeyword, false);
                break;
            case "ret":
                doc.setCharacterAttributes(wordStart, current.length(), styleKeyword, false);
                break;
            case "if":
                doc.setCharacterAttributes(wordStart, current.length(), styleKeyword, false);
                break;
            case "else":
                doc.setCharacterAttributes(wordStart, current.length(), styleKeyword, false);
                break;
            case "endif":
                doc.setCharacterAttributes(wordStart, current.length(), styleKeyword, false);
                break;
            case "while":
                doc.setCharacterAttributes(wordStart, current.length(), styleKeyword, false);
                break;
            case "not":
                doc.setCharacterAttributes(wordStart, current.length(), styleKeyword, false);
                break;
            case "do":
                doc.setCharacterAttributes(wordStart, current.length(), styleKeyword, false);
                break;
            case "end":
                doc.setCharacterAttributes(wordStart, current.length(), styleKeyword, false);
                break;
            case "break":
                doc.setCharacterAttributes(wordStart, current.length(), styleKeyword, false);
                break;
            case "print":
                doc.setCharacterAttributes(wordStart, wordEnd-wordStart, styleKeyword, false);
                break;
        }
    }

    public void syntaxHighlightWholeDocument(DefaultStyledDocument doc){
        try {
            String text = doc.getText(0, doc.getLength());
            String current;
            int wordStart = -1, wordEnd = 0;
            boolean comment = false;
            boolean number = false;
            for(int i = 0; i < text.length(); i++) {
                current = String.valueOf(text.charAt(i));
                if(current.equals(";"))
                    doc.setCharacterAttributes(i, 1, styleKeyword, false);
                if(wordStart == -1) {
                    if(current.equals("#")) {
                        comment = true;
                        wordStart = i;
                    }else if (current.matches("\\d")){
                        number = true;
                        wordStart = i;
                    }else if (current.matches("\\w")) {
                        wordStart = i;
                    }
                    continue;
                }else{
                    if(comment){
                        if(current.equals("\n")) {
                            wordEnd = i;
                        }else{
                            continue;
                        }
                    }else if (!current.matches("\\w") && !current.matches("\\d")) {
                        wordEnd = i;
                    }else
                        continue;
                }
                current = text.substring(wordStart, wordEnd);
                switch(current){
                    default:
                        if(number)
                            doc.setCharacterAttributes(wordStart, wordEnd-wordStart, styleNumber, false);
                        else if(comment)
                            doc.setCharacterAttributes(wordStart, wordEnd-wordStart, styleComment, false);
                        else
                            doc.setCharacterAttributes(wordStart, wordEnd-wordStart, styleWord, false);
                        break;
                    case "clear":
                        doc.setCharacterAttributes(wordStart, wordEnd-wordStart, styleKeyword, false);
                        break;
                    case "init":
                        doc.setCharacterAttributes(wordStart, wordEnd-wordStart, styleKeyword, false);
                        break;
                    case "copy":
                        doc.setCharacterAttributes(wordStart, wordEnd-wordStart, styleKeyword, false);
                        break;
                    case "incr":
                        doc.setCharacterAttributes(wordStart, wordEnd-wordStart, styleKeyword, false);
                        break;
                    case "decr":
                        doc.setCharacterAttributes(wordStart, wordEnd-wordStart, styleKeyword, false);
                        break;
                    case "proc":
                        doc.setCharacterAttributes(wordStart, wordEnd-wordStart, styleKeyword, false);
                        break;
                    case "ret":
                        doc.setCharacterAttributes(wordStart, wordEnd-wordStart, styleKeyword, false);
                        break;
                    case "if":
                        doc.setCharacterAttributes(wordStart, wordEnd-wordStart, styleKeyword, false);
                        break;
                    case "else":
                        doc.setCharacterAttributes(wordStart, wordEnd-wordStart, styleKeyword, false);
                        break;
                    case "endif":
                        doc.setCharacterAttributes(wordStart, wordEnd-wordStart, styleKeyword, false);
                        break;
                    case "while":
                        doc.setCharacterAttributes(wordStart, wordEnd-wordStart, styleKeyword, false);
                        break;
                    case "not":
                        doc.setCharacterAttributes(wordStart, wordEnd-wordStart, styleKeyword, false);
                        break;
                    case "do":
                        doc.setCharacterAttributes(wordStart, wordEnd-wordStart, styleKeyword, false);
                        break;
                    case "end":
                        doc.setCharacterAttributes(wordStart, wordEnd-wordStart, styleKeyword, false);
                        break;
                    case "break":
                        doc.setCharacterAttributes(wordStart, wordEnd-wordStart, styleKeyword, false);
                        break;
                    case "print":
                        doc.setCharacterAttributes(wordStart, wordEnd-wordStart, styleKeyword, false);
                        break;
                }
                comment = false;
                number = false;
                wordStart = -1;
            }
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
    }
}