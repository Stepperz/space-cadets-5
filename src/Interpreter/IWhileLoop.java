package Interpreter;

/**
 * Created by Ollie on 30/10/2014.
 */
public class IWhileLoop {

    public int startLine;
    public int endLine;

    public IWhileLoop(int start, int end){
        this.startLine = start;
        this.endLine = end;
    }

}
