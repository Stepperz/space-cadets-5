package Interpreter;

import java.util.Stack;

/**
 * Created by os4g14 on 30/10/14.
 */
public class Procedure {

    private int startLine;
    private Stack<IWhileLoop> whileLoops = new Stack<>();
    private int returnLine;

    public Procedure(int start){
        this.startLine = start;
    }

    public Procedure setReturnLine(int returnLine){
        this.returnLine = returnLine;
        return this;
    }

    public int getReturnLine() {
        return returnLine;
    }


    public int getStartLine(){
        return startLine;
    }

    public void pushWhileLoop(IWhileLoop loop){
        whileLoops.push(loop);
    }

    public IWhileLoop peekWhileLoop(int srcLineNumber){
        if(!whileLoops.isEmpty()){
            return whileLoops.peek();
        }else{
            System.err.println("Error: End statement found without a valid while loop on line " + srcLineNumber);
            System.exit(-1);
        }
        return null;
    }

    public IWhileLoop popWhileLoop(int srcLineNumber){
        if(!whileLoops.isEmpty()){
            return whileLoops.pop();
        }else{
            System.err.println("Error: break statement found not inside while loop on line " + srcLineNumber);
            System.exit(-1);
        }
        return null;
    }

    public Stack<IWhileLoop> getWhileLoops(){
        return whileLoops;
    }

    public Procedure copy(){
        return new Procedure(startLine);
    }

}
