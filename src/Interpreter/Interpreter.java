package Interpreter;

import IDE.EditorOutput;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

/**
 * Created by Ollie on 30/10/2014.
 */
public class Interpreter {

    private int srcLineNumber = 1;
    private String[] parts;

    private ArrayList<String> commandList;
    private HashMap<String, Integer> variables;

    private HashMap<String, Procedure> procedures;
    private Stack<IWhileLoop> whileLoops;
    private Stack<IIfStatement> ifStatements;
    private Stack<Procedure> callStack;

    private int branchDepth = 0;

    private String fileName = "NULL";

    private int[] breakPoints = null;

    private boolean finished = false;
    private boolean failure = false;
    private EditorOutput output;

    public Interpreter(String fileName, EditorOutput output) throws FileNotFoundException {
        this.fileName = fileName;
        this.output = output;

        parts = new String[4];

        variables = new HashMap<>();
        procedures = new HashMap<>();
        commandList = new ArrayList<>();
        whileLoops = new Stack<>();
        ifStatements = new Stack<>();
        callStack = new Stack<>();
    }

    public Interpreter(String fileName, int[] breakPoints, EditorOutput output) throws FileNotFoundException {
        this.fileName = fileName;
        this.output = output;

        parts = new String[4];

        variables = new HashMap<>();
        procedures = new HashMap<>();
        commandList = new ArrayList<>();
        whileLoops = new Stack<>();
        ifStatements = new Stack<>();
        callStack = new Stack<>();
    }

    public HashMap<String, Integer> getVariables(){
        return variables;
    }

    public boolean isFinished(){
        return finished;
    }

    public boolean hasFailed(){
        return failure;
    }

    public int getSrcLineNumber(){
        return srcLineNumber + 1;
    }

    //Called to start the compiler running
    public void run() throws IOException {
        if(fileName == null)
            return;

        mainPass();
    }

    //Called to start the compiler running
    public void debug(int[] breakPoints) throws IOException {
        this.breakPoints = breakPoints;
        if(fileName == null)
            return;

        mainPassDebug();
    }

    private void mainPassDebug() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        String line;

        while((line = reader.readLine()) != null) {
            line = line.trim();
            commandList.add(line);
        }

        reader.close();

        //Procedure identify stage
        srcLineNumber = 0;
        String[] parts;
        Procedure currentProcedure = null;
        while(true) {
            if(srcLineNumber == commandList.size())
                break;
            line = commandList.get(srcLineNumber);
            if(line.startsWith("proc") && line.endsWith(";")){
                parts = line.split(" ");
                if(parts.length == 2){
                    currentProcedure = new Procedure(srcLineNumber);
                    procedures.put(parts[1].split(";")[0], currentProcedure);
                }else{
                    output.println("Error: Invalid procedure declaration at line " + (srcLineNumber+1));
                    failure = true;
                    return;
                }
            }
            srcLineNumber++;
        }

        //Execution stage
        srcLineNumber = 0;

        while (true) {
            for(int i : breakPoints){
                if(srcLineNumber+1 == i){
                    return;
                }
            }
            if (srcLineNumber >= commandList.size()) {
                finished = true;
                break;
            }
            line = commandList.get(srcLineNumber);
            if (line.startsWith("hlt;")) {
                finished = true;
                break;
            }
            if(processInstruction(line) == -1)
                break;
            srcLineNumber++;
        }
    }

    public void continueDebug(int[] breakPoints){
        this.breakPoints = breakPoints;
        String line;
        boolean firstLine = true;
        while (true) {
            if(!firstLine)
                for(int i : breakPoints){
                    if(srcLineNumber+1 == i){
                        return;
                    }
                }
            if (srcLineNumber >= commandList.size()) {
                finished = true;
                break;
            }
            line = commandList.get(srcLineNumber);
            if (line.startsWith("hlt;")) {
                finished = true;
                break;
            }
            if(processInstruction(line) == -1)
                break;
            srcLineNumber++;
            firstLine = false;
        }
    }

    private void mainPass() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        String line;

        while((line = reader.readLine()) != null) {
            line = line.trim();
            commandList.add(line);
        }

        reader.close();

        //Procedure identify stage
        srcLineNumber = 0;
        String[] parts;
        Procedure currentProcedure = null;
        while(true) {
            if(srcLineNumber == commandList.size())
                break;
            line = commandList.get(srcLineNumber);
            if(line.startsWith("proc") && line.endsWith(";")){
                parts = line.split(" ");
                if(parts.length == 2){
                    currentProcedure = new Procedure(srcLineNumber);
                    procedures.put(parts[1].split(";")[0], currentProcedure);
                }else{
                    output.println("Error: Invalid procedure declaration at line " + (srcLineNumber+1));
                    failure = true;
                    return;
                }
            }
            srcLineNumber++;
        }

        //Execution stage
        srcLineNumber = 0;

        while (true) {
            if (srcLineNumber >= commandList.size()) {
                finished = true;
                break;
            }
            line = commandList.get(srcLineNumber);
            if (line.startsWith("hlt;")) {
                finished = true;
                break;
            }
            if(processInstruction(line) == -1)
                break;
            srcLineNumber++;
        }
    }

    private int processInstruction(String line){
        if(!line.isEmpty()) {
            if (line.startsWith("#")) {
                return 0;
            }

            if (!line.endsWith(";")) {
                output.println("Error: missing ';' at line " + (srcLineNumber+1));
                failure = true;
                return -1;
            }

            line = line.split(";")[0];

            parts = line.split(" ");

            switch (parts[0]) {
                default:
                    if(parts.length == 1) {
                        if (procedures.get(parts[0]) == null) {
                            if(parts[0].endsWith("--")) {
                                String var = parts[0].split("\\--")[0];
                                if (variables.get(var) == null) {
                                    variables.put(var, -1);
                                }else {
                                    variables.replace(var, variables.get(var) - 1);
                                }
                            }else if(parts[0].endsWith("++")) {
                                String var = parts[0].split("\\++")[0];
                                if (variables.get(var) == null) {
                                    variables.put(var, 1);
                                }else {
                                    variables.replace(var, variables.get(var) + 1);
                                }
                            }else {
                                output.println("Error: No valid procedure with name '" + parts[0] + "' at line " + (srcLineNumber+1));
                                failure = true;
                                return -1;
                            }
                        } else {
                            callStack.push(procedures.get(parts[0]).copy().setReturnLine(srcLineNumber));
                            srcLineNumber = callStack.peek().getStartLine() - 1;
                        }
                    }else if(parts.length == 3){
                        if(parts[1].equals("=")) {
                            if (variables.get(parts[0]) == null) {
                                variables.put(parts[0], 0);
                            }
                            if (variables.get(parts[2]) == null) {
                            } else {
                                variables.replace(parts[0], variables.get(parts[2]));
                            }
                        }else{
                            output.println("Error: Unrecognised instruction at line " + (srcLineNumber+1));
                            failure = true;
                            return -1;
                        }
                    }else if(parts.length == 5){
                        Integer v1 = null, v2 = null, v3 = null;
                        if (variables.get(parts[0]) == null) {
                            if(!canParse(parts[0]))
                                variables.put(parts[0], 0);
                            else
                                v1 = Integer.valueOf(parts[0]);
                        }else{
                            v1 = variables.get(parts[0]);
                        }
                        if (variables.get(parts[2]) == null) {
                            if(!canParse(parts[2]))
                                variables.put(parts[2], 0);
                            else
                                v2 = Integer.valueOf(parts[2]);
                        }else{
                            v2 = variables.get(parts[2]);
                        }
                        if (variables.get(parts[4]) == null) {
                            if(!canParse(parts[4]))
                                variables.put(parts[4], 0);
                            else
                                v3 = Integer.valueOf(parts[4]);
                        }else{
                            v3 = variables.get(parts[4]);
                        }
                        switch(parts[3]){
                            case "+":
                                v1 = v2 + v3;
                                variables.replace(parts[0], v1);
                                break;

                            case "-":
                                v1 = v2 - v3;
                                variables.replace(parts[0], v1);
                                break;

                            case "*":
                                v1 = v2 * v3;
                                variables.replace(parts[0], v1);
                                break;

                            case "/":
                                v1 = v2 / v3;
                                variables.replace(parts[0], v1);
                                break;
                        }
                    }else{
                        output.println("Error: Unrecognised instruction at line " + (srcLineNumber+1));
                        return -1;
                    }
                    break;

                case "init":
                    if (parts[2].equals("=")) {
                        try {
                            if (variables.get(parts[1]) == null) {
                                variables.put(parts[1], Integer.parseInt(parts[3]));
                            } else {
                                variables.replace(parts[1], Integer.parseInt(parts[3]));
                            }
                        } catch (NumberFormatException e) {
                            output.println("Error: Invalid number format exception at line " + (srcLineNumber+1));
                            failure = true;
                            return -1;
                        }
                    } else {
                        output.println("Error: Invalid init statement at line " + (srcLineNumber+1));
                        failure = true;
                        return -1;
                    }
                    break;

                case "clear":
                    if (variables.get(parts[1]) == null) {
                        variables.put(parts[1], 0);
                    } else {
                        variables.replace(parts[1], 0);
                    }
                    break;

                case "copy":
                    if (variables.get(parts[1]) == null) {
                        variables.put(parts[1], 0);
                    }

                    if (variables.get(parts[3]) == null) {
                        variables.put(parts[3], variables.get(parts[1]));
                    } else {
                        variables.replace(parts[3], variables.get(parts[1]));
                    }
                    break;

                case "incr":
                    if(parts.length == 2) {
                        if (variables.get(parts[1]) == null) {
                            variables.put(parts[1], 1);
                        } else {
                            int i = variables.get(parts[1]);
                            variables.replace(parts[1], i + 1);
                        }
                    }else{
                        output.println("Error: Invalid incr statement on line " + (srcLineNumber+1));
                        failure = true;
                        return -1;
                    }
                    break;

                case "decr":
                    if(parts.length == 2) {
                        if (variables.get(parts[1]) == null) {
                            variables.put(parts[1], 1);
                        } else {
                            int i = variables.get(parts[1]);
                            variables.replace(parts[1], i - 1);
                        }
                    }else{
                        output.println("Error: Invalid incr statement on line " + (srcLineNumber+1));
                        failure = true;
                        return -1;
                    }
                    break;

                case "proc":
                    break;

                case "ret":
                    if (callStack.size() > 0)
                        srcLineNumber = callStack.pop().getReturnLine();
                    else {
                        output.println("Error: Return statement found but the call-stack is empty on line " + (srcLineNumber+1));
                        failure = true;
                        return -1;
                    }
                    break;

                case "if":
                    if (variables.get(parts[1]) == null) {
                        variables.put(parts[1], 0);
                    }
                    IIfStatement ifSt = new IIfStatement();
                    int ifsToFind = 0;
                    int endLineNumber = srcLineNumber + 1;
                    while (true) {
                        if (commandList.get(endLineNumber).startsWith("if ")) {
                            ifsToFind++;
                        }
                        if (commandList.get(endLineNumber).equals("else;") && ifsToFind == 0) {
                            ifSt.setElsePos(endLineNumber);
                        }
                        if (commandList.get(endLineNumber).equals("endif;") && ifsToFind == 0) {
                            ifSt.setEndPos(endLineNumber);
                            break;
                        }
                        if (commandList.get(endLineNumber).equals("endif;"))
                            ifsToFind--;
                        endLineNumber++;
                        if (endLineNumber == commandList.size()) {
                            output.println("Error: Cannot find 'endif' for if statement on line " + (srcLineNumber+1));
                            failure = true;
                            return -1;
                        }
                    }
                    ifStatements.push(ifSt);
                    Integer left, right;
                    left = variables.get(parts[1]);
                    right = variables.get(parts[3]);
                    if (left == null) {
                        try {
                            left = Integer.parseInt(parts[1]);
                        } catch (NumberFormatException e) {
                            output.println("Error: Cannot find variable with name '" + parts[1] + "' on line " + (srcLineNumber+1));
                            failure = true;
                            return -1;
                        }
                    }
                    if (right == null) {
                        try {
                            right = Integer.parseInt(parts[3]);
                        } catch (NumberFormatException e) {
                            output.println("Error: Cannot find variable with name '" + parts[1] + "' on line " + (srcLineNumber+1));
                            failure = true;
                            return -1;
                        }
                    }
                    switch (parts[2]) {
                        default:
                            output.println("Error: Invalid if statement comparison '" + parts[2] + "' on line " + (srcLineNumber+1));
                            failure = true;
                            return -1;

                        case "<":
                            if (left >= right) {
                                srcLineNumber = ifStatements.peek().getElsePos();
                            }
                            break;

                        case ">":
                            if (left <= right) {
                                srcLineNumber = ifStatements.peek().getElsePos();
                            }
                            break;

                        case "==":
                            if (!left.equals(right)) {
                                srcLineNumber = ifStatements.peek().getElsePos();
                            }
                            break;

                        case ">=":
                            if (left < right) {
                                srcLineNumber = ifStatements.peek().getElsePos();
                            }
                            break;

                        case "<=":
                            if (left > right) {
                                srcLineNumber = ifStatements.peek().getElsePos();
                            }
                            break;

                        case "!=":
                            if (left.equals(right)) {
                                srcLineNumber = ifStatements.peek().getElsePos();
                            }
                            break;
                    }
                    break;

                case "else":
                    if (ifStatements.size() > 0) {
                        srcLineNumber = ifStatements.pop().getEndPos();
                    } else {
                        output.println("Error: else statement found but no if statement on line " + (srcLineNumber+1));
                        failure = true;
                        return -1;
                    }
                    break;

                case "endif":
                    break;

                case ("while"):
                    if (parts[2].equals("not") || parts[4].equals("do")) {
                        if (variables.get(parts[1]) == null) {
                            variables.put(parts[1], 0);
                        }
                        boolean addedAlready = false;
                        for (IWhileLoop w : whileLoops) {
                            if (w.startLine == srcLineNumber) {
                                addedAlready = true;
                            }
                        }
                        int whileLoopsToFind = 0;
                        if (!addedAlready) {
                            endLineNumber = srcLineNumber + 1;
                            while (true) {
                                if (commandList.get(endLineNumber).startsWith("while ")) {
                                    whileLoopsToFind++;
                                }
                                if (commandList.get(endLineNumber).equals("end;") && whileLoopsToFind == 0)
                                    break;
                                if (commandList.get(endLineNumber).equals("end;"))
                                    whileLoopsToFind--;
                                endLineNumber++;
                                if(endLineNumber == commandList.size()) {
                                    output.println("Error: Missing 'end' statement for while loop on line " + (srcLineNumber+1));
                                    failure = true;
                                    return -1;
                                }
                            }
                            if (callStack.isEmpty())
                                whileLoops.push(new IWhileLoop(srcLineNumber, endLineNumber));
                            else
                                callStack.peek().pushWhileLoop(new IWhileLoop(srcLineNumber, endLineNumber));
                        }
                        if (variables.get(parts[1]).equals(Integer.parseInt(parts[3]))) {
                            if (callStack.isEmpty())
                                srcLineNumber = whileLoops.pop().endLine;
                            else
                                srcLineNumber = callStack.peek().popWhileLoop(srcLineNumber).endLine;
                        }
                    }
                    break;

                case "break":
                    if (!whileLoops.isEmpty()) {
                        srcLineNumber = whileLoops.pop().endLine;
                    } else if(!callStack.isEmpty()){
                        srcLineNumber = callStack.peek().popWhileLoop(srcLineNumber).endLine;
                    }else{
                        output.println("Error: break statement not inside while loop on line " + (srcLineNumber+1));
                        failure = true;
                        return -1;
                    }
                    break;

                case "end":
                    if(whileLoops.size() > 0){
                        srcLineNumber = whileLoops.peek().startLine-1;
                    }

                    if (!whileLoops.isEmpty()) {
                        srcLineNumber = whileLoops.peek().startLine-1;
                    } else if(!callStack.isEmpty()){
                        srcLineNumber = callStack.peek().peekWhileLoop(srcLineNumber).startLine-1;
                    }else{
                        output.println("Error: End statement found without a valid while loop on line " + (srcLineNumber+1));
                        failure = true;
                        return -1;
                    }
                    break;

                case "print":
                    String outputLine = "";
                    for(int i = 1; i < parts.length; i++){
                        if(i < parts.length-1)
                            outputLine += parts[i] + " ";
                        else
                            outputLine += parts[i];
                    }
                    if(!outputLine.startsWith("\"") || !outputLine.endsWith("\"")){
                        output.println("Error: String literal must be enclosed in \" \" on line " + (srcLineNumber+1));
                        failure = true;
                        return -1;
                    }
                    output.println(outputLine.substring(1, outputLine.length()-1));
                    break;
            }
        }
        return 0;
    }

    public boolean canParse(String s){
        try{
            Integer.parseInt(s);
            return true;
        }catch(NumberFormatException e){
            return false;
        }
    }
}
