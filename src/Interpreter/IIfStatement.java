package Interpreter;

/**
 * Created by os4g14 on 30/10/14.
 */
public class IIfStatement {

    private int elsePos = 0;
    private int endPos = 0;

    public void setElsePos(int elsePos){
        this.elsePos = elsePos;
    }
    public void setEndPos(int endPos){
        this.endPos = endPos;
    }

    public int getElsePos(){
        if(elsePos == 0)
            return endPos;
        return elsePos;
    }

    public int getEndPos(){
        return endPos;
    }

}
